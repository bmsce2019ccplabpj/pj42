#include<stdio.h>
int main()
{
    int a[100],i,n,key,beg,end,mid,flag=0;
    printf("Enter number of elements in the array\n");
    scanf("%d",&n);
    printf("Enter elements of the array\n");
    
    for(i=0;i<n;i++)
        scanf("%d",&a[i]);
    
    printf("Enter element to be searched\n");
    scanf("%d",&key);
    
    beg=0;end=n-1;
    while(beg<=end)
    {
        mid=(beg+end)/2;
        if(key==a[mid])
            {flag=1;break;}
        else if(a[mid]<key)
            beg=mid+1;
        else
            end=mid-1;
    }
    if(flag==1)
        printf("Element is present in array at the position %d\n", mid+1);
    else
        printf("Element is absent in array\n");
return 0;
}