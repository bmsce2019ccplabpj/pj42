#include<stdio.h>
void input(int *a, int *b)
{
    printf("Enter a:");
    scanf("%d",a);
    printf("Enter b:");
    scanf("%d",b);
}
void swap(int *a, int *b)
{
    int c=*a;
    *a=*b;
    *b=c;
}
void output(int a,int b)
{
    printf("\na=%d\nb=%d\n",a,b);
}
int main()
{
    int a,b;
    input (&a,&b);
    swap(&a,&b);
    output(a,b);
    return 0;
}