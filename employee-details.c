#include <stdio.h>
struct dob
{
    int date,month,year;
};
struct employee
{
    char name[25],pos[25];
    int salary;
    struct dob birth;
};
void number(int *n)
{
    printf("Number of employees\n");
    scanf("%d",n);
}
void input(int n,struct employee a[n])
{
    int i;
    for(i=0;i<n;i++)
    {
        printf("Enter details for Employee %d\n",(i+1));
        printf("Name : ");
        scanf("%s", a[i].name);
        printf("Date Of Birth : ");
        scanf("%d/%d/%d",&a[i].birth.date,&a[i].birth.month,&a[i].birth.year);
        printf("Position : ");
        scanf("%s", a[i].pos);
        printf("Salary : ");
        scanf("%d",&a[i].salary);
    }
}
void output(int n,struct employee a[n])
{
    int i;
    for(i=0;i<n;i++)
        {
            printf("\nEmployee %d :\n",(i+1));
            printf("Name : %s\n",a[i].name);
            printf("Date Of Birth : %d/%d/%d\n",a[i].birth.date,a[i].birth.month,a[i].birth.year);
            printf("Position : %s\n",a[i].pos);
            printf("Salary : %d\n",a[i].salary);
        }
}
int main()
{
    int n;
    number(&n);
    struct employee a[n];
    input(n,a);
    output(n,a);
    return 0;
}