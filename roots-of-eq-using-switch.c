#include <stdio.h>
#include <math.h> 

int main()
{
    int z;
    float a,b,c;
    float x,y,i,d;
    printf("Enter values of a,b,c of quadratic equation a*x*x+b*x+c\n");
    scanf("%f%f%f",&a,&b,&c);
    d=(b*b)-(4*a*c);
    if (d>0)
        z=1;
    else if(d<0)
        z=2;
    else
        z=0;
        
    switch(z)
    {
        case 1:
            x = (-b+sqrt(d))/(2*a);
            y = (-b-sqrt(d))/(2*a);
            printf("The equation has real and unequal roots, %f & %f\n",x,y);
            break;
        case 2:
            x = y = -b/(2*a);
            i = sqrt(-d)/(2*a);
            printf("Two distinct complex roots exists, %.2f + i%.2f and %.2f - i%.2f\n",x,i,y,i);
            break;
        case 0:
            x = y = -b/(2*a);
            printf("Two equal and real roots exists, %.2f and %.2f\n", x,y);
            break;
    }
return 0;
}