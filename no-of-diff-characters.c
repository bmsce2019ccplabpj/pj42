#include<stdio.h>
int main()
{
    char a;
    int u=0,l=0,n=0;
    printf("Enter a character\n");
    scanf("%c",&a);
    do{
        if (a>='A' && a<='Z')
            u++;
        else if(a>='a' && a<='z')
            l++;
        else if(a>='0' && a<='9')
            n++;
        fflush(stdin);
        printf("Enter next character. Enter * to terminate\n");
        scanf("%c",&a);
    }
    while(a!='*');
    printf("The number of upper case characters are %d\n",u);
    printf("The number of lower case characters are %d\n",l);
    printf("The number of integers are %d\n",n);
    return 0;
}